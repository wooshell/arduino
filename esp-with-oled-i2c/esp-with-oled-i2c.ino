// LoLin module with I2C OLED module
// SDA = D2    SCK = D3

#include <U8g2lib.h>
#include <U8x8lib.h>
#include <Wire.h>
#include "OLED.h"

OLED display(4, 0);

void setup() {
  Serial.begin(9600);
  Serial.println("OLED test!");

  // Initialize display
  display.begin();

  // Test message
  display.print("Hello!");
  delay(3*1000);
}

void loop() {
    uint16_t adcval = analogRead(A0);
    display.print("ADC value 1234");
    Serial.print("ADC value ");
    Serial.println(adcval);
    delay(100);
}
