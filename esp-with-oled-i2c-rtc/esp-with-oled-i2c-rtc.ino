// LoLin module with I2C OLED module
// SDA = D2    SCK = D3

#include <U8g2lib.h>
#include <U8x8lib.h>
#include <Wire.h>
#include "OLED.h"
#include "RTClib.h"
RTC_DS1307 RTC;

OLED display(4, 0);
uint16_t oldval = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("OLED test!");

  // Initialize display
  display.begin();
  // Test message
  display.print("Hello!");
  delay(1000);
  RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  delay(1000);
}

void loop() {
    uint16_t adcval = analogRead(A0);
    char cstr[5];
    itoa(adcval, cstr, 10);
    if (adcval != oldval) 
     { display.clear(); };
    oldval = adcval;
    char outstr[20];
    strcpy(outstr, "ADC value ");
    strcat(outstr, cstr);
    display.print(outstr);
    Serial.print("ADC value ");
    Serial.println(adcval);
    delay(1000);
    DateTime now = RTC.now(); 
    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println(); 

}
