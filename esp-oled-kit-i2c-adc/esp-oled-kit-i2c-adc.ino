                 #include <U8g2lib.h>
#include "math.h"
#include <Wire.h>
#define DEVICE (0x48)
//U8g2 Constructor (for white board series!)
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 5, /* data=*/ 4);

// PCF8591T ADC is attached to the OLED i2c bus!
// SDA = D2  SCL = D1

byte _buff[6];
byte ana0 = 0, ana1 = 0, ana2 = 0, ana3 = 0;

u8g2_uint_t offset;     // current offset for the scrolling text
u8g2_uint_t width;      // pixel width of the scrolling text (must be lesser than 128 unless U8G2_16BIT is defined

void setup(void) {
  u8g2.begin();
  Serial.begin(9600);
  Serial.println("Init ADXL345 Demo");
  u8g2.setFont(u8g2_font_balthasar_titling_nbp_tf);
  u8g2.setFontMode(0);
  u8g2.firstPage();
}

void loop(void) {
    do {
    u8g2.clearBuffer();
    Wire.beginTransmission(DEVICE); // wake up PCF8591
    Wire.write(0x04); // control byte: reads ADC0 then auto-increment
    Wire.endTransmission(); // end tranmission
    Wire.requestFrom(DEVICE, 5);
    ana0=Wire.read();
    ana0=Wire.read();
    ana1=Wire.read();
    ana2=Wire.read();
    ana3=Wire.read();

    char outstr[25];
    memset(outstr, 0, 25);
    char val[4];
    strcat(outstr, "X:");
    itoa(ana0, val, 10);
    strcat(outstr, val);
    strcat(outstr, " Y:");
    itoa(ana1, val, 10);
    strcat(outstr, val);
    strcat(outstr, " Z:");
    itoa(ana2, val, 10);
    strcat(outstr, val);
    strcat(outstr, " Q:");
    itoa(ana3, val, 10);
    strcat(outstr, val);
    u8g2.drawUTF8(0, 16, outstr); // draw the text 
    delay(100);
   } while ( u8g2.nextPage() );
}
