// LoLin module with I2C OLED module
// SDA = D2    SCK = D3

#include <U8g2lib.h>
#include <U8x8lib.h>
#include <Wire.h>
#include "OLED.h"

OLED display(4, 0);
uint16_t oldval = 0;

void setup() {
  Serial.begin(9600);
  Serial.println("OLED test!");

  // Initialize display
  display.begin();

  // Test message
  display.print("Hello!");
  delay(1000);
}

void loop() {
    uint16_t adcval = analogRead(A0);
    char cstr[5];
    itoa(adcval, cstr, 10);
    if (adcval != oldval) 
     { display.clear(); };
    oldval = adcval;
    char outstr[20];
    strcpy(outstr, "ADC value ");
    strcat(outstr, cstr);
    display.print(outstr);
    Serial.print("ADC value ");
    Serial.println(adcval);
    delay(100);
}
