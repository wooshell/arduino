#include <U8g2lib.h>

//U8g2 Constructor (for white board series?)
//U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 5, /* data=*/ 4);
// Alternative board version. Uncomment if above doesn't work. (eg on black pcb series)
//U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 4, /* clock=*/ 14, /* data=*/ 2);

// test version for external 128x32 i2c display on sda=d2 sck=d3
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 0, /* data=*/ 4);

u8g2_uint_t offset;     // current offset for the scrolling text
u8g2_uint_t width;      // pixel width of the scrolling text (must be lesser than 128 unless U8G2_16BIT is defined
#include "RTClib.h"
RTC_DS1307 RTC;

void setup(void) {
  Serial.begin(9600);
  Serial.println("Starting...");
  u8g2.begin();
  u8g2.setFont(u8g2_font_logisoso24_tf);
  u8g2.setFontMode(0);
  RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}

void loop(void) {
  u8g2.firstPage();
    do {
    char tempstr[3] = "";
    char outstr[12] = "";
    char colon[2] = ":";
    char zero[2] = "0";
    DateTime now = RTC.now(); 
    itoa(now.hour(), tempstr, 10);
    if (now.hour() < 10) { strcat(outstr, zero); }
    strcat(outstr, tempstr);
    strcat(outstr, colon);
    itoa(now.minute(), tempstr, 10);
    if (now.minute() < 10) { strcat(outstr, zero); }
    strcat(outstr, tempstr);
    strcat(outstr, colon);
    itoa(now.second(), tempstr, 10);
    if (now.second() < 10) { strcat(outstr, zero); }
    strcat(outstr, tempstr);
    u8g2.drawUTF8(0, 24, outstr); // draw the text
//    Serial.println(outstr);
    delay(1000);
  } while ( u8g2.nextPage() );
}
