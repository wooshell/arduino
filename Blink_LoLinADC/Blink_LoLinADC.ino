// The LoLin boards do NOT follow the ESP pin numbering but regular Arduino labels,
// the LED is on pin 2 and NOT D2 like on the ESP01s minimodules
void setup() {
 Serial.begin(115200);
}

// the loop function runs over and over again forever
void loop() {
  uint16_t adcval = analogRead(A0);
  Serial.println(adcval);
delay(1000);
}
