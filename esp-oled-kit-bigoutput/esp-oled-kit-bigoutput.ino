#include <U8g2lib.h>

//U8g2 Constructor (for white board series?)
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 5, /* data=*/ 4);
// Alternative board version. Uncomment if above doesn't work. (eg on black pcb series)
//U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 4, /* clock=*/ 14, /* data=*/ 2);

// test version for external 128x32 i2c display on sda=d2 sck=d3
//U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 0, /* data=*/ 4);

u8g2_uint_t offset;     // current offset for the scrolling text
u8g2_uint_t width;      // pixel width of the scrolling text (must be lesser than 128 unless U8G2_16BIT is defined
uint16_t oldval = 0;

void setup(void) {
  Serial.begin(9600);
  delay(10);
  u8g2.begin();
  u8g2.setFont(u8g2_font_logisoso24_tf);
  u8g2.setFontMode(0);
  u8g2.firstPage();
  Serial.println();
  Serial.println();
}

void loop(void) {
    do {
    uint16_t adcval = analogRead(A0);
    if (oldval != adcval)
     {u8g2.clearBuffer();}
    oldval = adcval;
    char cstr[5];
    itoa(adcval, cstr, 10);
    char outstr[10];
    strcpy(outstr, "ADC:");
    strcat(outstr, cstr);
    u8g2.drawUTF8(0, 24, outstr); // draw the text
    delay(100);
  } while ( u8g2.nextPage() );
}
