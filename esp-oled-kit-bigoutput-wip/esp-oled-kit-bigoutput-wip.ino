#include <U8g2lib.h>
#include "math.h"
#include <Wire.h>
#define DEVICE (0x53)
//U8g2 Constructor (for white board series!)
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 5, /* data=*/ 4);

// I2C ADXL345 accelerometer is attached to the OLED bus!
// SDA = D2  SCL = D1

byte _buff[6];
char POWER_CTL = 0x2D;    //Power Control Register
char DATA_FORMAT = 0x31;
char DATAX0 = 0x32;    //X-Axis Data 0
char DATAX1 = 0x33;    //X-Axis Data 1
char DATAY0 = 0x34;    //Y-Axis Data 0
char DATAY1 = 0x35;    //Y-Axis Data 1
char DATAZ0 = 0x36;    //Z-Axis Data 0
char DATAZ1 = 0x37;    //Z-Axis Data 1

u8g2_uint_t offset;     // current offset for the scrolling text
u8g2_uint_t width;      // pixel width of the scrolling text (must be lesser than 128 unless U8G2_16BIT is defined

int Ax,Ay,Az = 0;

void readAccel() 
{
  uint8_t howManyBytesToRead = 6;
  readFrom( DATAX0, howManyBytesToRead, _buff); //read the acceleration data from the ADXL345
  
  // each axis reading comes in 10 bit resolution, ie 2 bytes. Least Significat Byte first!!
  // thus we are converting both bytes in to one
  // cave: esp8266 is 32bit, thus use short and not int as in Arduino examples
  short x = (((short)_buff[1]) << 8) | _buff[0];
  short y = (((short)_buff[3]) << 8) | _buff[2];
  short z = (((short)_buff[5]) << 8) | _buff[4];
  Ax = x; Ay = y; Az = z;
}

void writeTo(byte address, byte val) 
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // send register address
  Wire.write(val); // send value to write
  Wire.endTransmission(); // end transmission
}

// Reads num bytes starting from address register on device in to _buff array
void readFrom(byte address, int num, byte _buff[]) 
{
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.write(address); // sends address to read from
  Wire.endTransmission(); // end transmission
  Wire.beginTransmission(DEVICE); // start transmission to device
  Wire.requestFrom(DEVICE, num); // request 6 bytes from device

  int i = 0;
  while(Wire.available()) // device may send less than requested (abnormal)
  {
    _buff[i] = Wire.read(); // receive a byte
    i++;
  }
  Wire.endTransmission(); // end transmission
}
void setup(void) {
  u8g2.begin();
  Serial.begin(9600);
  Serial.println("Init ADXL345 Demo");
  u8g2.setFont(u8g2_font_balthasar_titling_nbp_tf);
  u8g2.setFontMode(0);
  u8g2.firstPage();
  // Put the ADXL345 into +/- 2G range by writing the value 0x01 to the DATA_FORMAT register.
  // FYI: 0x00 = 2G, 0x01 = 4G, 0x02 = 8G, 0x03 = 16G
  writeTo(DATA_FORMAT, 0x00);
  
  // Put the ADXL345 into Measurement Mode by writing 0x08 to the POWER_CTL register.
  writeTo(POWER_CTL, 0x08);
}

void loop(void) {
    do {
    u8g2.clearBuffer();
    char outstr[25];
    char val[4];
    strcat(outstr, "X:");
    itoa(Ax, val, 10);
    strcat(outstr, val);
    strcat(outstr, " Y:");
    itoa(Ay, val, 10);
    strcat(outstr, val);
    strcat(outstr, " Z:");
    itoa(Az, val, 10);
    strcat(outstr, val);
    u8g2.drawUTF8(0, 16, outstr); // draw the text 
    readAccel();  // read the x/y/z tilt
    delay(100);
    memset(outstr, 0, 20);
  } while ( u8g2.nextPage() );
}
