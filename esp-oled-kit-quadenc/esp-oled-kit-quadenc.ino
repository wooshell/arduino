// -----
// SimplePollRotator.ino - Example for the RotaryEncoder library.
// This class is implemented for use with the Arduino environment.
// Copyright (c) by Matthias Hertel, http://www.mathertel.de
// This work is licensed under a BSD style license. See http://www.mathertel.de/License.aspx
// More information on: http://www.mathertel.de/Arduino
// -----
// 18.01.2014 created by Matthias Hertel
// -----
#include <U8g2lib.h>
#include "math.h"
//U8g2 Constructor (for white board series!)
U8G2_SSD1306_128X32_UNIVISION_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ 16, /* clock=*/ 5, /* data=*/ 4);
u8g2_uint_t offset;     // current offset for the scrolling text
u8g2_uint_t width;      // pixel width of the scrolling text (must be lesser than 128 unless U8G2_16BIT is defined

// This example checks the state of the rotary encoder in the loop() function.
// The current position is printed on output when changed.

// Hardware setup:
// Attach a rotary encoder with output pins to A2 and A3.
// The common contact should be attached to ground.

#include <RotaryEncoder.h>

// Setup a RoraryEncoder for pins A2 and A3:
RotaryEncoder encoder(12, 14);

void setup()
{
  u8g2.begin();
  u8g2.setFont(u8g2_font_balthasar_titling_nbp_tf);
  u8g2.setFontMode(0);
  u8g2.firstPage();
  Serial.begin(9600);
  Serial.println("SimplePollRotator example for the RotaryEncoder library.");
} // setup()


// Read the current position of the encoder and print out when changed.
void loop()
{
  static int pos = 0;
  encoder.tick();
  char outstr[25];

  int newPos = encoder.getPosition();
  if (pos != newPos) {
    Serial.print(newPos);
    Serial.println();
    u8g2.clearBuffer();
    memset(outstr, 0, 25);
    char val[6];
    strcat(outstr, "POS: ");
    itoa(newPos, val, 10);
    strcat(outstr, val);
    u8g2.drawUTF8(0, 16, outstr); // draw the text 
    u8g2.nextPage();
    pos = newPos;
  } // if
} // loop ()

// The End

